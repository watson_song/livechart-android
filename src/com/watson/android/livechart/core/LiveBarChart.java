package com.watson.android.livechart.core;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

public class LiveBarChart extends BaseChart {
	private ArrayList<LiveBarChartData> mDatas;
	
	class LiveBarChartData {
		int color;
		float value;
		String label;
		
		public LiveBarChartData(int c, float v, String l) {
			this.color = c;
			this.value = v;
			this.label = l;
		}
	}
	
	private Paint mPaint;
	private Paint mLabelPaint = null;
    private float mSweep;
    private int mSelectedItem = -1;
    private float unitHeight = 1, largestValue = 0, vUnitHeight = 0, hUnitWidth = 0, rulerLargestValueWidth = 0;

    private static final float SWEEP_INC = 5;
    private static final float PADDING = 30;
    private static final float RULE_PIECES = 10;

	public LiveBarChart(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initializePaints();
	}

	public LiveBarChart(Context context, AttributeSet attrs) {
		super(context, attrs);
		initializePaints();
	}

	public LiveBarChart(Context context) {
		super(context);
		initializePaints();
	}
	
	@Override
	protected void initializePaints() {
		super.initializePaints();
		
		FRAME_WIDTH = 1000;
		
		mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(0x88FF0000);
        
        mLabelPaint = new Paint();
        mLabelPaint.setAntiAlias(true);
        mLabelPaint.setStyle(Paint.Style.STROKE);
        mLabelPaint.setTextSize(30);
        mLabelPaint.setStrokeWidth(0);
        
        mDatas = new ArrayList<LiveBarChartData>();
        
        mDatas.add(new LiveBarChartData(0xFFFF0000, 100, "Java"));
        mDatas.add(new LiveBarChartData(0xFF00FF00, 300, "Scala"));
        mDatas.add(new LiveBarChartData(0xFF0000FF, 600, "NodeJs"));
        mDatas.add(new LiveBarChartData(0xFF888888, 200, "Python"));
        mDatas.add(new LiveBarChartData(0xFFFF0000, 1000, "C#"));
        mDatas.add(new LiveBarChartData(0xFF00FF00, 400, "C++"));
        mDatas.add(new LiveBarChartData(0xFF0000FF, 550, "Ruby"));
        mDatas.add(new LiveBarChartData(0xFF888888, 879, "Flex"));
        
        mDatas.add(new LiveBarChartData(0xFFFF0000, 2000, "Javascript"));
        mDatas.add(new LiveBarChartData(0xFF00FF00, 430, "Delphi"));
        mDatas.add(new LiveBarChartData(0xFF0000FF, 520, "Go"));
        mDatas.add(new LiveBarChartData(0xFF888888, 379, "Groovy"));
        
        updateUIValue();
	}        
	
	private void updateUIValue() {
		for(LiveBarChartData chartData:mDatas) {
			if(chartData.value>largestValue) {
				largestValue = chartData.value;
			}
		}
		
		unitHeight = (FRAME_HEIGHT-2*PADDING-3*PADDING)/largestValue; //total ruler height
		vUnitHeight = unitHeight*(largestValue/RULE_PIECES);
		rulerLargestValueWidth = mLabelPaint.measureText(String.valueOf(largestValue));
		hUnitWidth = (FRAME_WIDTH - (rulerLargestValueWidth + 1.5f*PADDING) - 2*PADDING)/mDatas.size();
		mSweep = FRAME_HEIGHT-2*PADDING;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		drawRuler(canvas, mLabelPaint);
		
		if(mSelectedItem!=-1) {
			mSweep = 3*PADDING;
        }
		
		float startPointX = rulerLargestValueWidth + 1.5f*PADDING;
		for(int i=0;i<mDatas.size();i++) {
			LiveBarChartData chartData = mDatas.get(i);
			if(mSelectedItem==-1) {
        		mPaint.setColor(chartData.color);
        	}else {
        		if(mSelectedItem%mDatas.size()==i) {
        			mPaint.setColor(chartData.color);
        			mLabelPaint.setFakeBoldText(true);
        			float barValueWidth = mLabelPaint.measureText(String.valueOf(chartData.value));
        			canvas.drawText(String.valueOf(chartData.value), startPointX + i*hUnitWidth + (hUnitWidth-barValueWidth)/2, FRAME_HEIGHT - 3*PADDING - chartData.value*unitHeight, mLabelPaint);
        		}else {
        			mPaint.setColor(Color.LTGRAY);
        			mLabelPaint.setFakeBoldText(false);
        		}
        	}
			
			RectF chartBarRect = new RectF(startPointX + i*hUnitWidth + hUnitWidth/4f, Math.max(mSweep, FRAME_HEIGHT-2*PADDING - chartData.value*unitHeight), startPointX + (i+1)*hUnitWidth - hUnitWidth/4f, FRAME_HEIGHT-2*PADDING);
			canvas.drawRect(chartBarRect, mPaint);
			
			float labelOffsetX = (hUnitWidth-mLabelPaint.measureText(chartData.label))/2;
			if(labelOffsetX<0) {
				Path path = new Path();
				path.moveTo(startPointX + i*hUnitWidth, FRAME_HEIGHT-PADDING);
				path.lineTo(startPointX + (i+1)*hUnitWidth, FRAME_HEIGHT-PADDING);
				path.lineTo(startPointX + i*hUnitWidth, FRAME_HEIGHT);
				path.lineTo(startPointX + (i+1)*hUnitWidth, FRAME_HEIGHT);
				canvas.drawTextOnPath(chartData.label, path, 0, 0, mLabelPaint);
			}else {
				canvas.drawText(chartData.label, startPointX + i*hUnitWidth + labelOffsetX, FRAME_HEIGHT-PADDING, mLabelPaint);
			}
		}
		
		mSweep -= SWEEP_INC;
		if(mSweep<3*PADDING) {
			mSweep = FRAME_HEIGHT-2*PADDING;
		}else {
			invalidate();
		}
	}
	
	private void drawRuler(Canvas canvas, Paint paint) {
		paint.setFakeBoldText(false);
		
		Path path = new Path();
		paint.setStyle(Style.FILL);
		path.moveTo(rulerLargestValueWidth + 1.5f*PADDING, PADDING);
		path.lineTo(rulerLargestValueWidth + PADDING, 2*PADDING);
		path.lineTo(rulerLargestValueWidth + 2f*PADDING, 2*PADDING);
		path.lineTo(rulerLargestValueWidth + 1.5f*PADDING, PADDING);
		canvas.drawPath(path, paint);
		
		path.moveTo(FRAME_WIDTH-PADDING, FRAME_HEIGHT-2*PADDING);
		path.lineTo(FRAME_WIDTH-2f*PADDING, FRAME_HEIGHT-2.5f*PADDING);
		path.lineTo(FRAME_WIDTH-2f*PADDING, FRAME_HEIGHT-1.5f*PADDING);
		path.lineTo(FRAME_WIDTH-PADDING, FRAME_HEIGHT-2*PADDING);
		canvas.drawPath(path, paint);
		
		for(int i=0;i<RULE_PIECES;i++) {
			canvas.drawLine(rulerLargestValueWidth + PADDING, vUnitHeight*i+3*PADDING, rulerLargestValueWidth + 1.5f*PADDING, vUnitHeight*i+3*PADDING, paint);
			canvas.drawText(String.valueOf(largestValue/RULE_PIECES*(RULE_PIECES-i)), PADDING, vUnitHeight*i+3*PADDING+paint.getTextSize()/2, paint);
		}

		mLabelPaint.setStyle(Style.STROKE);
		canvas.drawLine(rulerLargestValueWidth + 1.5f*PADDING, PADDING, rulerLargestValueWidth +1.5f*PADDING, FRAME_HEIGHT-PADDING, mLabelPaint);
		canvas.drawLine(PADDING, FRAME_HEIGHT-PADDING-PADDING, FRAME_WIDTH-PADDING, FRAME_HEIGHT-PADDING-PADDING, mLabelPaint);
	}

    @Override
    public boolean onTouchEvent(MotionEvent event) {
    	Log.e("LivePieChart", event.toString());
    	mSelectedItem++;
    	if(mSelectedItem>mDatas.size())mSelectedItem = -1;
    	invalidate();
    	return super.onTouchEvent(event);
    }
}
