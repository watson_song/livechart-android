package com.watson.android.livechart.core;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;

public class BaseChart extends View {

    protected int FRAME_WIDTH = 800, FRAME_HEIGHT = 600;
    
    protected float[] mChartCenter;
    protected Paint mFramePaint;
    protected RectF mFrameRect;
    protected Shader mBG;     // background checker-board pattern
    protected boolean mDrawBoard = false, mDrawBGShadow = false;

	public BaseChart(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public BaseChart(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public BaseChart(Context context) {
		super(context);
	}
	
	protected void initializePaints() {
		if(mDrawBoard||mDrawBGShadow) {
			mFrameRect = new RectF(0, 0, FRAME_WIDTH, FRAME_HEIGHT);
			mFramePaint = new Paint();
			mFramePaint.setAntiAlias(true);
			mFramePaint.setStyle(Paint.Style.STROKE);
			mFramePaint.setStrokeWidth(0);
			if(mDrawBGShadow) {
				// make a ckeckerboard pattern
				Bitmap bm = Bitmap.createBitmap(new int[] { 0xFFFFFFFF, 0xFFCCCCCC, 0xFFCCCCCC, 0xFFFFFFFF }, 2, 2, Bitmap.Config.RGB_565);
				mBG = new BitmapShader(bm, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
				Matrix m = new Matrix();
				m.setScale(6, 6);
				mBG.setLocalMatrix(m);
			}
		}
	}
	
	@Override protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.WHITE);

        if(mDrawBGShadow) {
        	// draw the checker-board pattern
        	mFramePaint.setStyle(Paint.Style.FILL);
        	mFramePaint.setShader(mBG);
        	canvas.drawRect(mFrameRect, mFramePaint);
        }
        
        if(mDrawBoard) {
        	mFramePaint.setStyle(Paint.Style.STROKE);
        	mFramePaint.setShader(null);
        	//draw the outer frame
        	canvas.drawRect(mFrameRect, mFramePaint);
        }
	}
}
