package com.watson.android.livechart.core;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

public class LivePieChart extends BaseChart {
	
	class LivePieChartData {
		int color;
		float percentage;//0-1
		String label;
		
		public LivePieChartData(int c, float perc, String l) {
			this.color = c;
			this.percentage = perc;
			this.label = l;
		}
		
		public float getSweepAngle() {
			return 360*percentage;
		}
	}
	
	private Paint mPaint;
	private Paint mLabelPaint = null;
    private boolean mUseCenter = true;
    private RectF mChartOval;
    private float mSweep;
    private int mSelectedItem = -1;

    private static final float SWEEP_INC = 5;
    private static final float LABEL_PADDING = 20;
    private static final int CHART_WIDTH = 400;
    
    private ArrayList<LivePieChartData> mDatas;

	public LivePieChart(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initializePaints();
	}

	public LivePieChart(Context context, AttributeSet attrs) {
		super(context, attrs);
		initializePaints();
	}

	public LivePieChart(Context context) {
		super(context);
		initializePaints();
    }
	
	@Override
	protected void initializePaints() {
		super.initializePaints();

		mChartCenter = new float[]{FRAME_WIDTH/2f, FRAME_HEIGHT/2};
		
		mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(0x88FF0000);
        
        float chartHPadding = (FRAME_WIDTH-CHART_WIDTH)/2f;
        float chartVPadding = (FRAME_HEIGHT-CHART_WIDTH)/2f;
        mChartOval = new RectF(chartHPadding, chartVPadding, CHART_WIDTH+chartHPadding, CHART_WIDTH+chartVPadding);        
        
        mLabelPaint = new Paint();
        mLabelPaint.setAntiAlias(true);
        mLabelPaint.setStyle(Paint.Style.STROKE);
        mLabelPaint.setTextSize(30);
        mLabelPaint.setStrokeWidth(0);
        
        mDatas = new ArrayList<LivePieChartData>();
        
        mDatas.add(new LivePieChartData(0xFFFF0000, 0.1f, "Java"));
        mDatas.add(new LivePieChartData(0xFF00FF00, 0.05f, "Scala"));
        mDatas.add(new LivePieChartData(0xFF0000FF, 0.2f, "Node.js"));
        mDatas.add(new LivePieChartData(0xFF888888, 0.15f, "Python"));
        mDatas.add(new LivePieChartData(0xFFFF0000, 0.24f, "C#"));
        mDatas.add(new LivePieChartData(0xFF00FF00, 0.06f, "C++"));
        mDatas.add(new LivePieChartData(0xFF0000FF, 0.08f, "Ruby"));
        mDatas.add(new LivePieChartData(0xFF888888, 0.12f, "Flex"));
	}

    @Override protected void onDraw(Canvas canvas) {
    	super.onDraw(canvas);

        float sweepAngle = 0;
        float start = 0;
        if(mSelectedItem!=-1) {
        	mSweep = 360;
        }
        for(int i = 0;i< mDatas.size();i++) {
        	LivePieChartData chartData = mDatas.get(i);
        	if(mSelectedItem==-1) {
        		mPaint.setColor(chartData.color);
        	}else {
        		mPaint.setColor(mSelectedItem%mDatas.size()==i?chartData.color:Color.LTGRAY);
        		mLabelPaint.setFakeBoldText(mSelectedItem%mDatas.size()==i);
        	}
        	sweepAngle += chartData.getSweepAngle();
        	if(sweepAngle > mSweep) {
        		//draw the pie chart
        		canvas.drawArc(mChartOval, start, mSweep-start, mUseCenter, mPaint);
        		break;
        	}else {
        		drawLabel(canvas, mLabelPaint, (mSelectedItem!=-1&&mSelectedItem%mDatas.size()==i)?chartData.label+" "+chartData.percentage*100+"%":chartData.label, mChartCenter, CHART_WIDTH/2f, (sweepAngle+start)/2f);
        		
        		//draw the pie chart
        		canvas.drawArc(mChartOval, start, chartData.getSweepAngle(), mUseCenter, mPaint);
        		start = sweepAngle;
        	}
        }

        mSweep += SWEEP_INC;
        if (mSweep > 360) {
            mSweep -= 360;
        }else {
        	invalidate();
        }
    }
    
    private void drawLabel(Canvas canvas, Paint paint, String label, float[] center, float radius, float angle) {
    	float[] result = new float[2];
    	float textHeight = paint.getTextSize();
		
    	float cosOffset;
    	float sinOffset;
		if(angle<90) {
			cosOffset = (float)(LABEL_PADDING*Math.cos(Math.toRadians(angle)));
			sinOffset = (float)(LABEL_PADDING*Math.sin(Math.toRadians(angle)));
    		result[0] = (float) (center[0] + radius*Math.cos(Math.toRadians(angle)));
    		result[1] = (float) (center[1] + radius*Math.sin(Math.toRadians(angle)));
    		canvas.drawLine(result[0], result[1], result[0] + cosOffset, result[1] + sinOffset, paint);
    		canvas.drawLine(result[0] + cosOffset, result[1] + sinOffset, result[0] + cosOffset + LABEL_PADDING, result[1] + sinOffset, paint);
    		canvas.drawText(label, result[0] + cosOffset + LABEL_PADDING + LABEL_PADDING/2f, result[1] + sinOffset + textHeight/2f, paint);
    	}else if(angle<180) {
    		cosOffset = (float)(LABEL_PADDING*Math.cos(Math.toRadians(angle)));
    		sinOffset = (float)(LABEL_PADDING*Math.sin(Math.toRadians(angle)));
    		float textWidth = paint.measureText(label);
    		result[0] = (float) (center[0] + radius*Math.cos(Math.toRadians(angle)));
    		result[1] = (float) (center[1] + radius*Math.sin(Math.toRadians(angle)));
    		canvas.drawLine(result[0], result[1], result[0] + cosOffset, result[1] + sinOffset, paint);
    		canvas.drawLine(result[0] + cosOffset, result[1] + sinOffset, result[0] + cosOffset - LABEL_PADDING, result[1] + sinOffset, paint);
    		canvas.drawText(label, result[0] + cosOffset - textWidth - LABEL_PADDING - LABEL_PADDING/2f, result[1] + sinOffset + textHeight/2f, paint);
    	}else if(angle<270) {
    		cosOffset = (float)(LABEL_PADDING*Math.cos(Math.toRadians(angle - 180)));
    		sinOffset = (float)(LABEL_PADDING*Math.sin(Math.toRadians(angle - 180)));
    		float textWidth = paint.measureText(label);
    		result[0] = (float) (center[0] - radius*Math.cos(Math.toRadians(angle-180)));
    		result[1] = (float) (center[1] - radius*Math.sin(Math.toRadians(angle-180)));
    		canvas.drawLine(result[0], result[1], result[0] - cosOffset, result[1] - sinOffset, paint);
    		canvas.drawLine(result[0] - cosOffset, result[1] - sinOffset, result[0] - cosOffset - LABEL_PADDING, result[1] - sinOffset, paint);
    		canvas.drawText(label, result[0] - cosOffset - textWidth - LABEL_PADDING - LABEL_PADDING/2f, result[1] - sinOffset + textHeight/2f, paint);
    	}else {
    		cosOffset = (float)(LABEL_PADDING*Math.cos(Math.toRadians(360 - angle)));
    		sinOffset = (float)(LABEL_PADDING*Math.sin(Math.toRadians(360 - angle)));
    		result[0] = (float) (center[0] + radius*Math.cos(Math.toRadians(360-angle)));
    		result[1] = (float) (center[1] - radius*Math.sin(Math.toRadians(360-angle)));
    		canvas.drawLine(result[0], result[1], result[0] + cosOffset, result[1] - sinOffset, paint);
    		canvas.drawLine(result[0] + cosOffset, result[1] - sinOffset, result[0] + cosOffset + LABEL_PADDING, result[1] - sinOffset, paint);
    		canvas.drawText(label, result[0] + cosOffset + LABEL_PADDING + LABEL_PADDING/2f, result[1] - sinOffset + textHeight/2f, paint);
    	}
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event) {
    	Log.e("LivePieChart", event.toString());
    	if(!(mSweep>SWEEP_INC&&mSweep<360)) {
    		mSelectedItem++;
    		if(mSelectedItem>mDatas.size())mSelectedItem = -1;
    		invalidate();
    	}
    	return super.onTouchEvent(event);
    }

}
